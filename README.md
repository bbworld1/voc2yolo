# V2Y: A simple VOC to YOLO converter script.

Need to convert your Pascal/VOC format XML annotations to YOLO's text format? No problem!

Just run these commands:

`wget https://gitlab.com/bbworld1/voc2yolo/raw/master/v2y.py`

`python3 v2y.py`

It'll automatically convert all VOC XML files in the directory to YOLO TXT.

NOTE: This script may catch other, regular XML files not in PASCAL VOC format.
Please remove other, regular XML files from the directory before converting.

Requirements:

v2y requires Python 3 and BeautifulSoup4.
