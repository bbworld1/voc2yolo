from bs4 import BeautifulSoup
import os

with open("classes.txt", "a+") as classptr:
    classptr.seek(0)
    classes = classptr.read().split()
    print(classes)
    classptr.seek(2)
    for i in os.listdir():
        if i.split(".")[-1] == "xml":
            with open(i) as f:
                # Reformat string so that BeautifulSoup reads it right
                voc = f.read()
                voc = voc.replace("<name>", "<objname>")
                voc = voc.replace("</name>", "</objname>")

                soup = BeautifulSoup(voc, 'lxml-xml')

                # Parse Image Size
                width = int(soup.find("width").string)
                height = int(soup.find("height").string)

                print(width, height)

                with open(i.split(".")[0] + ".txt", "w") as fout:
                    for obj in soup.find_all("object"):
                        if str(obj.objname.string) not in classes:
                            # Add object to classes.txt
                            classes.append(str(obj.objname.string))
                            classptr.write(str(obj.objname.string) + "\n")

                        # Convert values to something YOLO likes
                        name = str(classes.index(obj.objname.string))
                        xmin = int(obj.xmin.string)
                        ymin = int(obj.ymin.string)
                        xmax = int(obj.xmax.string)
                        ymax = int(obj.ymax.string)

                        bndbox = [round(((xmax + xmin)/2)/width, 6),
                                  round(((ymax + ymin)/2)/height, 6),
                                  round((xmax - xmin)/width, 6),
                                  round((ymax - ymin)/height, 6)]


                        print(name, bndbox)
                        fout.write(name + " " + " ".join(map(str, bndbox)) + "\n")
